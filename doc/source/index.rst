********************
DataWhys Tutorial
********************

.. topic:: Overview

    This short tutorial provides the basics for getting started with DataWhys.
    The goal here is to get you set up and able to start working with DataWhys's advanced AI in a Python environment.

.. contents::
    :depth: 3


Installing the DataWhys SDK
=================================
Use ``pip`` to install the DataWhys SDK

    .. code:: shell

        pip install datawhys

From here you can start using ``datawhys`` in your python code

    .. code:: python

        import datawhys as dw


Using the DataWhys SDK
=================================
With DataWhys installed, you are now ready to charge full force, almost. We have one administrative item to make this work. That's the API Key which gives permissions to the SDK
to make API requests to the DataWhys API. You haven't received a DataWhys SDK API Key from a representative of DataWhys yet, please contact them to start the process.

Initializing the SDK with your keys is as follows

    .. code :: python

        dw.api_key = "<API KEY>"

        # Verify authorization & connection status
        dw.api.api_test()


.. note::

    You'll need a Pandas DataFrame to run a solve. In our examples this will be represented by ``df``.
    If you need a fake DataFrame you can use the following snippet to create one.

        .. code :: python

            import numpy as np
            import pandas as pd
            df = pd.DataFrame(np.random.randint(0,100,size=(100, 4)), columns=list('ABCD'))


Once successfully authenticated, you can begin using the solve method and related utilities.


    .. code:: python

        from datawhys.solver import solve
        from datawhys.metrics import score
        from datawhys.datautils import apply_rule

        rule = solve(df)
        rule_data = apply_rule(df, rule)

        print(rule)
        print(rule_data)
        print(score(df, rule=rule))


What you have completed here is the core of DataWhys. There are many applications of DataWhys's unique AI but the benefit of working with the SDK in a Python
programming interface is that implementing all the various applications is now much easier.


Interacing Directly with API Server
===================================
If you want to interact directly with the DataWhys API servers you are able to. Rather than creating a Solver instance you can call api functions directly. Reference
the code block below or the api module in the API Reference.

    .. code-block:: python

        import datawhys as dw

        dw.api_key = "<API-KEY>"

        dw.api.api_test()

        task = dw.api.solve_start(outcome="SepalLength", target="max", data=data)
        result = dw.api.solve_result(id=task['id'])

        print(result)



*************
API Reference
*************

API
====


.. automodule:: datawhys.api
    :members:

Data Utils
==========
.. automodule:: datawhys.datautils
    :members:

Diagnostics
===========
.. automodule:: datawhys.diagnostics
    :members:

Metrics
=======
.. automodule:: datawhys.metrics
    :members:



Solver
======
.. automodule:: datawhys.solver
    :members:


**********
Deprecated
**********

Prescriber
===========
.. autoclass:: datawhys.prescriber.Solver


DataWhysFrame
==============

.. automodule:: datawhys.core.frame
   :members:

DataWhysSeries
==============

.. automodule:: datawhys.core.series
   :members:



MetaWhys
==========
MetaWhys is DataWhys's feature generation engine.  Whenever it learns a rule, it transforms the rule into a feature
variable, which is added to the original dataset.  Then, it iteratively repeats the process, finding new rules that
build off of and further refine previously found feature rules.

.. automodule:: datawhys.core.meta
   :members:

