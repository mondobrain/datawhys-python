# How to contribute to the DataWhys Python SDK

`datawhys-sdk` is an ordinary Python package. You can install it with `pip
install -e .` into some virtualenv, edit the sourcecode and test out your
changes manually.

# Code Standards
## Python (PEP8 / black)
*datawhys* follows the PEP8 standard. [Black](https://black.readthedocs.io/en/stable/) and [Flake8](http://flake8.pycqa.org/en/latest/) are used to ensure formatting is consistent throughout the project.

CI will run checks against both Black and Flake8 to ensure consistency. You can run these checks yourself:
```
black datawhys
git diff master -u -- "*.py" | flake8 --diff
```
The above will auto-format your code. Many editors ([vscode](https://black.readthedocs.io/en/stable/editor_integration.html#visual-studio-code), [pycharm](https://black.readthedocs.io/en/stable/editor_integration.html#pycharm-intellij-idea), [etc.](https://black.readthedocs.io/en/stable/editor_integration.html#)) have plugins that support black and flake8 automatically.

## Import order
*datawhys* users [isort](https://pypi.org/project/isort/) to standardise import formatting across the project.

## Pre-Commit
We encourage you to use [pre-commit hooks](https://pre-commit.com/) to automatically run `black`, `flake8`, & `isort` when you make a git commit. This can be done by installing `pre-commit`:
```
pip install pre-commit

# or for macOS users
brew install pre-commit
```

and then running
```
pre-commit install
```
from the root of this repository. Styling checks will now be applied each time you commit. If needed, you can skip these checks with `git commit --no-verify`

## Running tests
Tests can be run locally simply via pytest.
```
pytest datawhys
```

