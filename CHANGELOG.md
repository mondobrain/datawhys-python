# v0.4.1
## 03/21/2022
### New
* Introduce a `fit_tree` method to perform a modified exhaustive solve

# v0.4.0
## 02/14/2022
### Breaking Changes
* The SDK now uses API key based authentication. Previously issued keys will no longer work. Reach out to your DataWhys representative to get new keys

### New
* Use API key base auth [DEV-24]

### Fixed
* Make timeout param optional and add max_cycles param [SDK-90]

# v0.3.2
## 11/24/2021
### New
* Initial proxy support [SDK-89](https://datawhys.atlassian.net/browse/SDK-89)

# v0.3.1
## 11/9/2021
### Fixed
* Use correct Auth0 audience

### Docs
* Fix documentation generation dependencies

# v0.3.0
## 10/28/2021
### New
* Rebrand as DataWhys [SDK-86](https://datawhys.atlassian.net/browse/SDK-86)
* Track data drift [SDK-81](https://datawhys.atlassian.net/browse/SDK-81)
* Add what-if analysis [SDK-82](https://datawhys.atlassian.net/browse/SDK-82)

# v0.2.0
## 7/13/2021
### New
* Implement K-Fold cross-validation [SDK-62](https://datawhys.atlassian.net/browse/SDK-62)
* Allow auth to be disabled [SDK-84](https://datawhys.atlassian.net/browse/SDK-84)
* Generate readable rules [SDK-83](https://datawhys.atlassian.net/browse/SDK-83)
* Add bias insights [PROD-61](https://datawhys.atlassian.net/browse/PROD-61)

### Improved
* Ability to apply rule to single data point [SDK-77](https://datawhys.atlassian.net/browse/SDK-77)

### Docs
* Update SDK docstrings [SDK-85](https://datawhys.atlassian.net/browse/SDK-85)

# v0.1.8
## 4/23/2021
### Fixed
* Pass kwargs to solve result

# v0.1.7
## 4/5/2021
### Improved
* Various updates/fixes to solve/score functions [PROD-60](https://datawhys.atlassian.net/browse/PROD-60) [PROD-63](https://datawhys.atlassian.net/browse/PROD-63)

# v0.1.6
## 3/30/2021
### New
* Add meta solves as part of `solver` module [SDK-74](https://datawhys.atlassian.net/browse/SDK-74)

### Fixed
* Allow minor version changes to `pandas` & `numpy`
* Require `networkx`

### Chores
* ci: lock pycodestyle version

# v0.1.5
## 3/11/2021
### Fixed
* Allow disabling SSL cert verification

# v0.1.4
## 3/3/2021
### New
* Add balanced rule generation to prescriber [SDK-71](https://datawhys.atlassian.net/browse/SDK-71)
* New method `partial_scores` [SDK-57](https://datawhys.atlassian.net/browse/SDK-57)

### Improved
* Return diagnostics in `solve` & discarded_rules in `prescriber` [SDK-59](https://datawhys.atlassian.net/browse/SDK-59)

### Fixed
* Better error handling for sklearn label encoding/decoding [SDK-68](https://datawhys.atlassian.net/browse/SDK-68)
* Correct the encoding behavior of continuous scoring [SDK-75](https://datawhys.atlassian.net/browse/SDK-75)

# v0.1.3
## 2/18/2021
### New
* Exhaustive ruleset generation [SDK-60](https://datawhys.atlassian.net/browse/SDK-60)
* Add ability to apply multiple rules [SDK-63](https://datawhys.atlassian.net/browse/SDK-63)

### Fixed
* Stop exhaustive solves on empty rule
* Key error when applying empty rule(s) [SDK-70](https://datawhys.atlassian.net/browse/SDK-70)

### Documentation
* Showcase new SDK UX as primary documentation

# v0.1.2
## 1/25/2021
### New
* New module `diagnostics` [SDK-56](https://mondobrain.atlassian.net/browse/SDK-56)
* Add `sample` method to `datautils`
* Create metrics module [SDK-51](https://mondobrain.atlassian.net/browse/SDK-51)

### Fixed
* Remove `None` values in request parameters
* Create `is_discrete` & `is_continuous` utils; remove usage of var_type in utils

### Testing
* Increase coverage of new methods

# v0.1.1
## 12/15/2020
### New
* Create solver module and solve method [SDK-50](https://mondobrain.atlassian.net/browse/SDK-50)

### Chores
* Remove typing from MetaMondo and pydantic

### Documentation
* Add MetaMondo to docs [SDK-55](https://mondobrain.atlassian.net/browse/SDK-55)

# v0.1.0
## 12/10/2020
Project versioning reset to more acurately indicate beta status

### New
* Changelog started/reset
