# flake8: noqa
from ._base import (
    load_bank_risk_date,
    load_bronchial_risk,
    load_iris,
    load_jets,
    load_mazda,
    load_political_behavior,
    load_shampoo,
    load_titanic,
)
