from pandas.testing import assert_frame_equal
import pytest

from datawhys.datautils import apply_rule, apply_rules


def test_apply_rule(iris):
    rule = {"PetalLength": {"lo": 0.1, "hi": 5.2}, "Name": "Iris-setosa"}

    left = iris.copy()
    left = left[(left.PetalLength >= 0.1) & (left.PetalLength <= 5.2)]
    left = left[left.Name == "Iris-setosa"]

    right = apply_rule(iris, rule)

    assert_frame_equal(left, right)

    # Check inverse rule
    rule = {"Name": "Iris-setosa"}
    left = iris.copy()
    left = left[left.Name != "Iris-setosa"]

    assert_frame_equal(left, apply_rule(iris, rule, inverse=True))


def test_apply_empty_rule(iris):
    with pytest.raises(ValueError):
        apply_rule(iris, {})
    with pytest.raises(ValueError):
        apply_rule(iris, {}, inverse=True)


def test_apply_rules(iris):
    rules = [
        {"PetalWidth": {"lo": 0.1, "hi": 0.6}},
        {"PetalWidth": {"lo": 1.8, "hi": 2.5}},
    ]

    cond_setosa = (iris["PetalWidth"] >= 0.1) & (iris["PetalWidth"] <= 0.6)
    cond_virginica = (iris["PetalWidth"] >= 1.8) & (iris["PetalWidth"] <= 2.5)

    # check "and"
    left = iris.loc[cond_setosa & cond_virginica]
    right = apply_rules(iris, rules, operation="and")
    assert_frame_equal(left, right)

    # check inverse "and"
    left = iris.loc[~(cond_setosa & cond_virginica)]
    right = apply_rules(iris, rules, inverse=True, operation="and")
    assert_frame_equal(left, right)

    # check "or"
    left = iris.loc[cond_setosa | cond_virginica]
    right = apply_rules(iris, rules, operation="or")
    assert_frame_equal(left, right)

    # check inverse "or"
    left = iris.loc[~(cond_setosa | cond_virginica)]
    right = apply_rules(iris, rules, inverse=True, operation="or")
    assert_frame_equal(left, right)


def test_apply_empty_rules(iris):
    with pytest.raises(ValueError):
        apply_rules(iris, [])
    with pytest.raises(ValueError):
        apply_rules(iris, [], inverse=True)
