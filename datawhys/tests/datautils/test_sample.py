import numpy as np
import pandas as pd
import pandas._testing as tm

from datawhys.datautils import sample
from datawhys.datautils._sample import _oversampled_sample, _uniform_stratified_sample


def test_sample():
    rs = 1

    # A 100 -> 199
    colA = range(100, 200)

    # B - 3 modalities, y less than 15%
    colB = np.full(100, "x")
    colB[range(20, 30)] = "y"
    colB[range(50, 100)] = "z"

    # C - 2 modalities
    colC = np.full(100, "a")
    colC[range(10, 20)] = "b"

    df = pd.DataFrame({"A": colA, "B": colB, "C": colC})

    # Check that samples applies a normal sample for continuous
    # Also ensures we check that we default to first col
    sample1 = sample(df, 10, random_state=rs)
    tm.assert_frame_equal(sample1, df.sample(10, random_state=rs))

    # Check that sample applies the uniform_stratified sample when target is unspecified
    sample2 = sample(df, 10, outcome="B", random_state=rs)
    tm.assert_frame_equal(
        sample2, _uniform_stratified_sample(df, 10, "B", random_state=rs)
    )

    # Check that sample applies the oversampled sample when target is less than 15%
    sample3 = sample(df, 10, outcome="B", target="y", random_state=rs)
    tm.assert_frame_equal(
        sample3, _oversampled_sample(df, 10, "B", "y", random_state=rs)
    )

    # Check that sample applies oversampled when 2 modalities and alternate is less
    # than 15%
    sample4 = sample(df, 10, outcome="C", target="a", random_state=rs)
    tm.assert_frame_equal(
        sample4, _oversampled_sample(df, 10, "C", "a", random_state=rs)
    )

    # Check that sample applies a normal sample for discrete vars where the target and
    # alternate is greater than 15%
    sample5 = sample(df, 10, outcome="B", target="x", random_state=rs)
    tm.assert_frame_equal(sample5, df.sample(10, random_state=rs))

    # Check that a copy is returned when floor is higher than row count
    sample6 = sample(df, 10, floor=999, random_state=rs)
    tm.assert_frame_equal(sample6, df)
