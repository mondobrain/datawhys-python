from numpy.testing import assert_array_equal
import pandas as pd
import pytest

from datawhys.core.series import DataWhysSeries
from datawhys.error import InvalidTargetError


def test_eries_extends_pandas(iris):
    assert isinstance(iris.SepalLength, DataWhysSeries)
    assert isinstance(iris.SepalLength, pd.Series)


def test_series_var_type(iris):
    assert iris.SepalLength.var_type == "continuous"
    assert iris.Name.var_type == "discrete"


def test_series_classes(iris):
    assert_array_equal(
        iris.Name.classes, ["Iris-setosa", "Iris-versicolor", "Iris-virginica"]
    )


def test_series_target_class(iris):
    assert iris.SepalLength.target_class is None
    assert iris.Name.target_class is None

    iris.SepalLength.target_class = "max"
    iris.Name.target_class = "Iris-setosa"

    assert iris.SepalLength.target_class == "max"
    assert iris.Name.target_class == "Iris-setosa"


def test_series_target_class_raises_InvalidTargetError(iris):
    with pytest.raises(InvalidTargetError):
        iris.SepalLength.target_class = "doesnotexist"

    with pytest.raises(InvalidTargetError):
        iris.Name.target_class = "doesnotexist"


def test_series_case_point(iris):
    series = iris.SepalLength

    assert series.case_point is None

    series.case_point = 2
    assert series.case_point == 2
