import pandas as pd

from datawhys.core.frame import DataWhysFrame


def test_whys_df_extends_pandas(iris):
    assert isinstance(iris, DataWhysFrame)
    assert isinstance(iris, pd.DataFrame)


def test_whys_df_filtering(iris):
    pass
