from numpy import nan
from numpy.testing import assert_array_equal
import pytest

from datawhys.utils.utilities import (
    DDTransformer,
    decode_column,
    decode_dataframe,
    encode_column,
    encode_dataframe,
)


@pytest.fixture
def encoder(iris):
    return DDTransformer(iris)


@pytest.fixture
def titanic_encoder(titanic):
    return DDTransformer(titanic)


def test_encode_column(iris, encoder, titanic, titanic_encoder, subtests):
    with subtests.test(msg="discrete"):
        series = iris.Name

        assert_array_equal(
            series.unique(), ["Iris-setosa", "Iris-versicolor", "Iris-virginica"]
        )

        encoded = encode_column(series, encoder)

        assert_array_equal(encoded.unique(), ["0", "1", "2"])

    with subtests.test(msg="continuous"):
        series = iris.SepalLength
        assert_array_equal(series[0:5].tolist(), [5.1, 4.9, 4.7, 4.6, 5.0])

        encoded = encode_column(series, encoder)

        assert_array_equal(encoded[0:5].tolist(), [8, 6, 4, 3, 7])

    with subtests.test(msg="with nulls"):
        series = titanic.boat
        assert_array_equal(series[0:5].tolist(), ["TWO", "ELEVEN", nan, nan, nan])
        assert series.isnull().any()

        encoded = encode_column(series, titanic_encoder)

        assert_array_equal(encoded[0:5].tolist(), ["26", "11", nan, nan, nan])
        assert encoded.isnull().any()


def test_decode_column(iris, encoder):
    original = iris.Name.copy()
    encoded = encode_column(iris.Name, encoder)
    decoded = decode_column(encoded, encoder)

    assert_array_equal(original, decoded)


def test_encode_dataframe(iris, encoder):
    assert_array_equal(iris.values[0].tolist(), [5.1, 3.5, 1.4, 0.2, "Iris-setosa"])
    assert_array_equal(iris.values[-1].tolist(), [5.9, 3.0, 5.1, 1.8, "Iris-virginica"])

    encoded = encode_dataframe(iris, encoder)

    assert_array_equal(encoded.values[0].tolist(), [8, 14, 4, 1, "0"])
    assert_array_equal(encoded.values[-1].tolist(), [16, 9, 27, 14, "2"])


def test_decode_dataframe(iris, encoder):
    original = iris.copy()
    encoded = encode_dataframe(iris, encoder)
    decoded = decode_dataframe(encoded, encoder)

    assert_array_equal(original, decoded)
