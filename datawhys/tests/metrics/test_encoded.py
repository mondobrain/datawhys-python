import numpy as np

from datawhys.metrics import encoded_mean


def test_encoded_mean():
    a = np.array([10, 20, 30])
    assert encoded_mean(a) == 1
