from datawhys.metrics import target_metrics


def test_target_metrics(iris, subtests):
    with subtests.test(msg="discrete"):
        metrics = target_metrics(iris.Name)
        assert metrics == {
            "mean": 0.3333333333333333,
            "std": 0.47140452079103173,
            "size": 150,
            "size_above_mean": 50,
        }

    with subtests.test(msg="continuous"):
        metrics = target_metrics(iris.SepalLength)
        assert metrics == {
            "mean": 5.843333333333334,
            "std": 0.8253012917851409,
            "size": 150,
            "size_above_mean": 70,
        }

        metrics1 = target_metrics(iris.SepalLength, target="min")
        assert metrics1 == {
            "mean": -5.843333333333334,
            "std": 0.8253012917851409,
            "size": 150,
            "size_above_mean": 0,
        }
