import pytest

from datawhys.metrics import partial_scores, score


def test_score(iris, subtests):
    with subtests.test(msg="rule_base"):
        with subtests.test(msg="discrete"):
            rule = {"PetalWidth": {"lo": 0.1, "hi": 0.6}}

            sc = score(iris, outcome="Name", rule=rule)
            assert sc == 10.000000000000002

        with subtests.test(msg="continuous"):
            rule = {
                "PetalLength": {"lo": 5.2, "hi": 6.9},
                "PetalWidth": {"lo": 1.6, "hi": 2.5},
            }

            sc = score(iris, outcome="SepalLength", rule=rule)
            assert sc == 7.225921818355716

    with subtests.test(msg="errors"):
        with pytest.raises(ValueError) as excinfo:
            score(iris)

        assert "one of" in str(excinfo.value) and "must be set" in str(excinfo.value)

        with pytest.raises(ValueError) as excinfo:
            score(iris, population=iris, rule={})

        assert "only one of" in str(excinfo.value)

    with subtests.test(msg="population_based"):
        sc = score(iris.sample(10, random_state=0), population=iris)
        assert sc == 0.8201499558174168

        sc = score(iris.sample(0, random_state=0), population=iris)
        assert sc == 0


def test_partial_score(iris, subtests):
    with subtests.test(msg="1-condition"):
        rule = {"PetalWidth": {"lo": 0.1, "hi": 0.6}}
        sc = partial_scores(iris, rule, outcome="Name")
        assert sc == {"PetalWidth": 10.000000000000002}

    with subtests.test(msg="n-conditions"):
        rule = {
            "PetalLength": {"lo": 5.2, "hi": 6.9},
            "PetalWidth": {"lo": 1.6, "hi": 2.5},
        }
        sc = partial_scores(iris, rule, outcome="Name")
        assert sc == {
            "PetalWidth": 0.06108642329968017,
            "PetalLength": 1.037000311274804,
        }
