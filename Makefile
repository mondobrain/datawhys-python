.PHONY : develop build clean clean_pyc lint-diff black test

all: develop

clean:
	-python setup.py clean --all

clean_pyc:
	-find . -name '*.py[co]' -exec rm {} \;

clean_dist:
	rm -rf dist
	rm -rf *.egg-info
	rm -rf .pytest_cache

build: clean_pyc
	python setup.py build_ext --inplace

package: clean_dist
	python setup.py sdist bdist_wheel

lint-diff:
	git diff development --name-only -- "*.py" | xargs flake8

black:
	black datawhys

test:
	pytest datawhys

develop: build
	python -m pip install --no-build-isolation -e .
